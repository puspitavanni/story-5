from django.urls import path 
from django.conf.urls import url

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.index, name="index"),
    path('search/<str:query>/', views.search, name="search"),
]