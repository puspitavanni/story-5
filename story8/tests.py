from django.http import response
from django.test import TestCase, Client
from django.test import client
from django.urls import resolve

from .views import index, search

# Create your tests here.

class TestStory8(TestCase):
    
    #TestView
    def test_view_is_using_index_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index)

    def test_view_is_using_search_func(self):
        found = resolve('/story8/search/olaf/')
        self.assertEqual(found.func, search)


    #TestURL
    def test_url_index_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_url_search_is_exist(self):
        response = Client().get('/story8/search/frozen/')
        self.assertEqual(response.status_code, 200)

    #TestTemplate
    def test_template_using_index(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/buku.html')
    