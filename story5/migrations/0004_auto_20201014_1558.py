# Generated by Django 3.1.2 on 2020-10-14 08:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0003_auto_20201014_1432'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='year',
            field=models.CharField(choices=[('2020/2021', '2020/2021'), ('2021/2022', '2021/2022'), ('2022/2023', '2022/2023')], default='2019/2020', max_length=9),
        ),
    ]
