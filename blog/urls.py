from django.urls import path
from django.conf.urls import url

from . import views

app_name = 'blog'

urlpatterns = [
    url(r'^project$', views.project, name="project"),  #path('project/', views.project, name="project")
    url(r'^experience$', views.exp, name="exp"),
    url(r'^list_blog$', views.list_blog, name="list_blog"),
    url(r'^first_blog$', views.first_blog, name="first_blog"),
    url(r'^story3$', views.story3, name="story3"),
    url(r'^story1$', views.story1, name="story1"),
    path('', views.index, name="index"),
]