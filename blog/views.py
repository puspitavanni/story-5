from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'blog/index.html')

def story1(request):
    return render(request, 'blog/story1.html')

def story3(request):
    return render(request, 'blog/story3.html')

def first_blog(request):
    return render(request, 'blog/first_blog.html')

def list_blog(request):
    return render(request, 'blog/list_blog.html')

def exp(request):
    return render(request, 'blog/exp.html')

def project(request):
    return render(request, 'blog/project.html')