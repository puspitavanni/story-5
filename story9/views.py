from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import authenticate, login, logout

from .models import Profile
from .forms import UserForm
# Create your views here.

# Terinspirasi dari Tugas Kelompok pertama saya

def akunlogin(request):
    if not request.user.is_authenticated:
        authform = AuthenticationForm()

        if request.method == 'POST':
            authform = AuthenticationForm(data=request.POST.copy())

            if authform.is_valid():
                user = authform.get_user()
                if user != None:
                    login(request, authform.get_user())
                    return redirect('story9:profile')

        context = { 
            'active': 'login', 
            'form': authform 
            }
        return render(request, 'story9/login.html', context)
    
    else:
        return redirect('/')

def akunlogout(request):
    logout(request)
    return redirect('story9:login')

def akunregister(request):
    if not request.user.is_authenticated:
        registerform = UserCreationForm()

        if request.method == 'POST':
            registerform = UserCreationForm(request.POST)
            
            if registerform.is_valid():
                user = registerform.save()
                Profile.objects.create(user = user)
                return redirect('story9:login')
        
        context = { 
            'active': 'register', 
            'form': registerform
            }
        return render(request, 'story9/register.html', context)    
    
    else:
        return redirect('/')


#fungsi di bawah ini diedit dari Tugas Kelompok pertama saya

def profile(request): 
    return redirect('story9:profile_dua')
    

def hasilprofile(request): # ambil data user
    get_data = None
    try:
        get_data = Profile.objects.get(user = request.user)
    except:
        get_data = Profile.objects.create(user = request.user)

    context = {'data':get_data}
    return render(request, 'story9/hasil.html', context)

def updateprofile(request, pk): #update data user
    get_data = Profile.objects.get(id=pk)
    get_profile = UserForm(instance = get_data)
    
    if request.method == "POST":
        get_profile = UserForm(request.POST, instance=get_data)
        if get_profile.is_valid():
            get_profile.edit_obj(get_data)
            return redirect('story9:profile_dua')
    context = {'edit':get_profile}
    return render(request, 'story9/edit.html', context)

