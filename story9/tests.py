from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from django.urls import resolve, reverse

from .views import akunlogin, akunlogout, akunregister, profile, hasilprofile, updateprofile
from .models import Profile

# Create your tests here.

#TestView
class TestStory9(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('story9:login'))
        self.assertEqual(response.status_code, 200)

    def test_logout_url(self):
        tes = [('/story9/login/', 302)]
        response = self.client.get('/story9/logout/', follow = True)
        self.assertEquals(response.redirect_chain, tes)

    def test_login_url_get_noauth(self):
        resp = self.client.get('/story9/login/')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'story9/login.html')

    def test_register_url_get_noauth(self):
        response = self.client.get('/story9/register/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/register.html')

    def test_login_url_post_invalid(self):
        response = self.client.post('/story9/login/', {'username': 'punnyy', 'kacangkentang': 'kentanggoreng'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_register_url_post_invalid(self):
        response = self.client.post('/story9/register/', {'username': 'punnyy', 'kacangkentang': 'kentanggoreng'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/register.html')


    def test_login_url_auth(self):
        User.objects.create_user(username = 'test', password = 'kentanggoreng')
        self.client.login(username = 'test', password = 'kentanggoreng')
        tes = [('/', 302)]
        response = self.client.get('/story9/login/', follow = True)
        self.assertEquals(response.redirect_chain, tes)
        
    def test_register_url_auth(self):
        User.objects.create_user(username = 'test', password = 'kentanggoreng')
        self.client.login(username = 'test', password = 'kentanggoreng')
        tes = [('/', 302)]
        response = self.client.get('/story9/register/', follow = True)
        self.assertEquals(response.redirect_chain, tes)

    def test_profile_using_func_profile(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, profile)

    def test_hasil_profile_using_func_profile_dua(self):
        found = resolve('/story9/hasilprofile/')
        self.assertEqual(found.func, hasilprofile)

    def test_edit_profile_using_func_update_profile(self):
        found = resolve('/story9/editprofile/1/')
        self.assertEqual(found.func, updateprofile)






