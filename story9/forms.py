from django import forms
from django.contrib.auth.models import User
from .models import Profile

class UserForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['nama', 'umur', 'alamat',]

    def edit_obj(self,profile_obj):
        profile_obj.nama = self.cleaned_data['nama']
        profile_obj.umur = self.cleaned_data['umur']
        profile_obj.alamat = self.cleaned_data['alamat']
        profile_obj.save()
