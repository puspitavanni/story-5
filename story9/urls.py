from django.urls import path
from . import views

app_name = "story9"

urlpatterns = [
    
    path('', views.profile, name ='profile'),
    path('hasilprofile/', views.hasilprofile, name = 'profile_dua'),
    path('editprofile/<int:pk>/', views.updateprofile , name = 'update_profile'),
    path('login/', views.akunlogin, name='login'),
    path('logout/', views.akunlogout, name='logout'),
    path('register/', views.akunregister, name='register')
]