from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Activity, Member

class TestStory6(TestCase):
    def setUp(self):
        self.client = Client()
        self.activity_url = reverse("story_6:activity")
        self.test_activity = Activity.objects.create(
            activity = "tidur"
        )
        self.test_member = Member.objects.create(
            member = "vanni",
            aktivitas = self.test_activity
        )
        self.test_delete_url = reverse("story_6:activityDelete",args=[self.test_activity.id])
        self.test_member_delete_url = reverse("story_6:memberDelete",args=[self.test_member.id])

    def test_activity_GET(self):
        response = self.client.get(self.activity_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story_6/index.html")

    def test_activity_POST(self):
        response = self.client.post(self.activity_url, {
            "activity" : "tidur"
        }, follow=True)
        self.assertContains(response, "tidur")

    def test_member_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "lili",
            "aktivitas" : self.test_activity.id,
        }, follow=True)
        self.assertContains(response, "lili")

    def test_notValid_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "lili",
            "aktivitas" : " ",
        }, follow=True)
        self.assertContains(response, "Input kamu tidak sesuai!")

    def test_member_DELETE(self):
        response = self.client.get(self.test_member_delete_url, follow=True)
        print(response.content)
        self.assertContains(response, "berhasil dihapus")

    def test_activity_DELETE(self):
        response = self.client.get(self.test_delete_url, follow=True)
        self.assertContains(response, "berhasil dihapus")

    # URL TEST
    def test_url_is_exist_activity(self):
        response = Client().get('/activity/')
        self.assertEqual(response.status_code, 200)

    # TEMPLATE TEST
    def test_template_story6_using_index_template(self):
        response = Client().get('/activity/')
        self.assertTemplateUsed(response, 'story_6/index.html')


    # MODEL TEST
    def test_model_activity_create(self):
       new_activity = Activity.objects.create(activity = "Tidur")

       count_all_available_activity = Activity.objects.all().count()
       self.assertEqual(count_all_available_activity, 2)


    def test_model_member_create(self):
        new_activity = Activity.objects.create(activity = "Tidur")
        new_member = Member.objects.create(member="vanni", aktivitas=new_activity)

        count_all_available_peserta = Member.objects.all().count()
        self.assertEqual(count_all_available_peserta, 2)

    def test_model_member_delete_cascade(self):
        new_activity = Activity.objects.create(activity = "Tidur")
        new_member = Member.objects.create(member="vanni", aktivitas=new_activity)
        new_activity.delete()

        count_all_available_activity = Activity.objects.all().count()
        count_all_available_member = Member.objects.all().count()
        self.assertEqual(count_all_available_activity, 1)
        self.assertEqual(count_all_available_member, 1)

    def test_model_member_delete(self):
        new_activity = Activity.objects.create(activity = "Tidur")
        new_member = Member.objects.create(member="vanni", aktivitas=new_activity)
        new_member.delete()

        count_all_available_member = Member.objects.all().count()
        self.assertEqual(count_all_available_member, 1)

    def test_model_activity_delete(self):
        new_activity = Activity.objects.create(activity = "Tidur")
        new_activity.delete()

        count_all_available_activity = Activity.objects.all().count()
        self.assertEqual(count_all_available_activity, 1)