$(".judul").click(function() {
    $(this).parent().next().slideToggle(500);   // slide atau menurunkan dropdown dengan kecepatan 500
})


$(".up").click(function() {
    var current = $(this).parent().parent().parent()
    var currentClass = 'accordion'+current.css('order')
    var prevClass = 'accordion'+ parseInt(current.css('order') - 1)   // mengurangi order dengan satu 
    if (parseInt(current.css('order')) > 1) {   // kondisi jika order telah sampai 1 (awal), maka tidak dapat dilakukan up
        $('.' + prevClass).addClass(currentClass)
        $('.' + prevClass).removeClass(prevClass)   
        $(this).parent().parent().parent().removeClass(currentClass) // menghilangkan currentClass (dengan order awal)
        $(this).parent().parent().parent().addClass(prevClass)      // menambah atau membuat class baru yaitu previousClass yang mana order nya 
                                                                        // telah dikurangi satu agar naik ke atas 
    }
})

$(".down").click(function() {
    var current = $(this).parent().parent().parent()
    var currentClass = 'accordion'+current.css('order')
    var nextClass = 'accordion'+ parseInt(parseInt(current.css('order')) + 1)    // menambah order dengan satu
    if (parseInt(current.css('order')) < 5) {   // kondisi jika order telah sampai 5 (akhir), maka tidak bisa dilakukan down
        $('.' + nextClass).addClass(currentClass)   
        $('.' + nextClass).removeClass(nextClass)   
        $(this).parent().parent().parent().removeClass(currentClass)    // menghilangkan currentClass (dengan order awal)
        $(this).parent().parent().parent().addClass(nextClass)          // menambah atau membuat class baru yaitu previousClass yang mana order nya 
                                                                        // telah dikurangi satu agar naik ke atas 
    }
})

