from django.test import TestCase, LiveServerTestCase
from django.urls import resolve, reverse
from django.test import Client

# Create your tests here.
from .views import accordion

class TestStory7(TestCase):

    #TestURL
    def test_url_is_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    #TestView
    def test_view_in_using_accordion_function(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, accordion)

    #TestTemplate
    def test_template_using_accordion_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/accordion.html')


    

