// FUNGSI DROPDOWN
$(".judul").click(function() {
    $(this).parent().next().slideToggle(500);   // menurunkan dropdown dengan kecepatan 500
})


// FUNGSI DOWN
$(".down").click(function() {
    var current = $(this).parent().parent().parent() //ambil class accordion dengan current order 
    var currentClass = 'accordion'+current.css('order')
    var nextClass = 'accordion'+ parseInt(parseInt(current.css('order')) + 1)    // menambah order dengan satu
    $('.' + nextClass).addClass(currentClass)   
    $('.' + nextClass).removeClass(nextClass) 
    if (parseInt(current.css('order')) < 5) {   // kondisi jika order telah sampai 5 (akhir), maka tidak bisa dilakukan down 
        $(this).parent().parent().parent().removeClass(currentClass)    // menghilangkan currentClass (dengan order awal)
        $(this).parent().parent().parent().addClass(nextClass)          // menambah atau membuat class baru yaitu previousClass yang mana order nya                                                 // telah dikurangi satu agar naik ke atas 
    }
})

//FUNGSI UP
$(".up").click(function() {
    var current = $(this).parent().parent().parent()
    var currentClass = 'accordion'+current.css('order')
    var prevClass = 'accordion'+ parseInt(current.css('order') - 1)   // mengurangi order dengan satu 
    $('.' + prevClass).addClass(currentClass)
    $('.' + prevClass).removeClass(prevClass)
    if (parseInt(current.css('order')) > 1) {   // kondisi jika order telah sampai 1 (awal), maka tidak dapat dilakukan up   
        $(this).parent().parent().parent().removeClass(currentClass) // menghilangkan currentClass (dengan order awal)
        $(this).parent().parent().parent().addClass(prevClass)      // menambah atau membuat class baru yaitu previousClass yang mana order nya                                                                // telah dikurangi satu agar naik ke atas 
    }
})
